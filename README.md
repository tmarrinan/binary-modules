### Bitbucket Uploader ###

Uses Node.js and cURL to upload files to Bitbucket repository "Downloads" page.


##### Usage #####

```
curl -O https://bitbucket.org/tmarrinan/binary-modules/raw/master/bitbucket_upload.js
node bitbucket_upload.js <user_name> <password> <repository> <file_to_upload>

```

##### Example #####

```
curl -O https://bitbucket.org/tmarrinan/binary-modules/raw/master/bitbucket_upload.js
node bitbucket_upload.js tmarrinan secret123 tmarrinan/binary-modules my_file.zip

```